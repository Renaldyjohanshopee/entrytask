import {FILTERED_SIZE, ADD_COMMENT, CHANGE_START, CHANGE_END, GET_FILTER, RESET_FILTER, GET_POST, GET_POSTS, CLEAR_POST, CLEAR_POSTS, POST_ERROR, GET_SCROLL,CLEAR_SCROLL, PAGE_INCREMENT,SCROLL_END, LOADING, CHANGE_CHANNEL, CHANGE_DATE, FILTER_TRUE} from '../actions/types';

const initialState = {
  post: null,
  posts:[],
  scroller:[],
  pagination:1,
  per:5,
  end:false,
  loading: true,
  error:{},
  date:null,
  channel:null,
  startDate:null,
  endDate:null,
  filtered:false,
  filteredSize:null
}

export default function(state = initialState, action){
  const {type, payload} = action;

  switch(type) {
    case FILTERED_SIZE:
      return {
        ...state,
        filteredSize: payload,
      };
    case ADD_COMMENT:
      return {
        ...state,
        post: { ...state.post, comments: payload },
        loading: false
      };
    case CHANGE_START:
      return{
        ...state,
        startDate:payload
      }
    case CHANGE_END:
      return{
        ...state,
        endDate:payload
      }
    case FILTER_TRUE:
      return{
        ...state,
        filtered:true
      }
    case GET_FILTER:
      return{
        ...state,
        scroller:payload,
        loading:false
      }
    case RESET_FILTER:
      return{
        ...state,
        scroller:[],
        pagination:1,
        end:false,
        date:null,
        channel:null,
        startDate:null,
        endDate:null,
        filtered:false,
        filteredSize:null,
        loading:true
      }
    case CHANGE_CHANNEL:
      return{
        ...state,
        channel:payload
      }
    case CHANGE_DATE:
      return{
        ...state,
        date:payload
      }    
    case LOADING:
      return{
        ...state,
        loading:true
      }
    case SCROLL_END:
      return{
        ...state,
        end:true,
        loading:false
      }
    case PAGE_INCREMENT:
      return{
        ...state,
        pagination: payload,
        loading:false
      }
    case GET_SCROLL:
      return{
        ...state,
        scroller: payload,
        loading:false
      }
    case CLEAR_SCROLL:
      return{
        ...state,
        scroller:[],
        pagination:1,
        loading:false,
        end:false,
      }      
    case GET_POST: 
      return{
        ...state,
        post: payload,
        loading:false
      };
    case GET_POSTS: 
      return{
        ...state,
        posts: payload,
        loading:false
      };
    case POST_ERROR:
      return{
        ...state,
        error: payload,
        loading:false
      };
    case CLEAR_POST:
      return{
        ...state,
        post:null,
        loading:false
      }
    case CLEAR_POSTS:
      return{
        ...state,
        posts:null,
        loading:false
      }
      default:
      return state;
  }
}