import { DRAWER_CLOSE, DRAWER_OPEN} from '../actions/types';

const initialState = {
  drawer:false
}

export default function(state = initialState, action){
  const {type} = action;
  switch(type){
    case DRAWER_OPEN:
      return{
        drawer:true
      }
    case DRAWER_CLOSE:
      return{
        drawer:false
      }
      default:
      return state;
  }
}