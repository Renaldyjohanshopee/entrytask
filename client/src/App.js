import React, {useEffect} from 'react';
import './App.scss';
import './font/SourceSansPro-Bold.ttf';
import './font/SourceSansPro-SemiBold.ttf';
import 'react-router';
import Main from './pages/main';
import Login from './pages/login';
import Event from './pages/event';
import { Route, Switch } from 'react-router';
import ProtectedRoute from './common/protectedRoutes';
import Alert from './pages/alert';
import {Provider} from 'react-redux';
import store from './store';
import Profile from './pages/profile';
import{loadUser} from './actions/auth';

const App = () => {
  useEffect(()=>{
    store.dispatch(loadUser());
  }, []);
  return(
    <Provider store={store}>
    <div className="App">
      <Alert />
      <Switch>
        <Route exact path="/" component={Login} />
        <ProtectedRoute exact path='/dashboard' component={Main} />
        <ProtectedRoute path='/event' component={Event} />
        <ProtectedRoute path='/profile' component={Profile} />
        <Route path="*" component={() => "404 NOT FOUND"} />
      </Switch>
    </div>
  </Provider>
  )
};

export default App;
