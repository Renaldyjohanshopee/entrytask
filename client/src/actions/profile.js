import axios from 'axios';

import{
  GET_PROFILE,
  GET_PROFILES,
  PROFILE_ERROR,
  CLEAR_PROFILE
} from './types';

export const getCurrentProfile = () => async dispatch =>{
  try {
    dispatch({
      type:CLEAR_PROFILE
    })
    const res = await axios.get('/api/profile/me');

    dispatch({
      type:GET_PROFILE,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type:PROFILE_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const getProfileById = userId => async dispatch =>{
  try {
    const res = await axios.get(`/api/profile/user/${userId}`);

    dispatch({
      type:GET_PROFILE,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type:PROFILE_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const getProfiles = () => async dispatch => {
  dispatch({ type: CLEAR_PROFILE });

  try {
    const res = await axios.get('/api/profile');

    dispatch({
      type: GET_PROFILES,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};
