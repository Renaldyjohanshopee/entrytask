import{
  DRAWER_CLOSE,
  DRAWER_OPEN
} from './types';

export const closeDrawer = () => async dispatch =>{
  dispatch({
    type:DRAWER_CLOSE
  });
};

export const openDrawer = () => async dispatch =>{
  dispatch({
    type:DRAWER_OPEN
  })
};