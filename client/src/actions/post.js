import axios from 'axios';

import{
  GET_POSTS,
  POST_ERROR,
  CLEAR_POSTS,
  GET_POST,
  GET_SCROLL,
  PAGE_INCREMENT,
  SCROLL_END,
  LOADING,
  CHANGE_CHANNEL,
  CHANGE_DATE,
  RESET_FILTER,
  CLEAR_SCROLL,
  GET_FILTER,
  DRAWER_CLOSE,
  FILTER_TRUE,
  CHANGE_START,
  CHANGE_END,
  ADD_COMMENT,
  FILTERED_SIZE
} from './types';

export const changeStart = (e) => dispatch => {
  try {
    dispatch({
      type:CHANGE_START,
      payload:e
    });
      
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const changeEnd = (e) => dispatch => {
  try {
    dispatch({
      type:CHANGE_END,
      payload:e
    });
      
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const filterTrue = () => dispatch => {
  try {
    dispatch({
      type:FILTER_TRUE
    });
      
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const resetFilter = () => dispatch => {
  try {
    dispatch({
      type:RESET_FILTER
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const filterPosts = (endScroll, filterDate, start, end, filterChannel, scroller, page, per, filter) => async dispatch =>{
  try {
    dispatch({
      type:LOADING,
      payload:[]
    })
    dispatch({
      type:DRAWER_CLOSE
    })
    
    let allParam = '';
    const paramA = `limit=${per}`;
    const paramB = `page=${page}`;
    const paramC = !filterChannel ? '' : filterChannel!=="All channel" ? `&channel=${filterChannel.replace(/\s/g, '')}` : '';
    const paramD = !filterDate ? '' : filterDate!=="anytime" ? `&date=${filterDate.replace(/\s/g, '')}` : ''; 
    if (filterDate==="later"){
      const paramE = `&start=${start}`;
      const paramF = `&end=${end}`;
      allParam = `${paramA}&${paramB}${paramC}${paramD}${paramE}${paramF}`;
    }else{
      allParam = `${paramA}&${paramB}${paramC}${paramD}`;
    }
    const res = await axios.get(`/api/posts/filteredpagedposts?${allParam}`);
    const check = await axios.get(`/api/posts/filteredposts?${allParam}`);
    const filterData = check.data.length;
    let feedback = [...scroller, ...res.data.result] 
    const limit = page*per;
    let nextpage;
    if(limit>check.data.length-1){
      nextpage = page;
      if(endScroll===true){
        feedback = scroller;
      }
      dispatch({
        type:SCROLL_END,
        payload:"true"
      })
    }
    else nextpage = page+1;

    dispatch({
      type:GET_FILTER,
      payload: feedback
    });
    dispatch({
      type:PAGE_INCREMENT,
      payload:nextpage
    });
    dispatch({
      type:FILTERED_SIZE,
      payload:filterData
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });    
  }
}

export const clearScroll = () => dispatch =>{
  try {
    dispatch({
      type:CLEAR_SCROLL
    });
      
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const changeDate = (date) => dispatch =>{
  dispatch({
    type:CHANGE_DATE,
    payload:date
  });
};

export const changeChannel = (channel) => dispatch =>{
  dispatch({
    type:CHANGE_CHANNEL,
    payload:channel
  });
};

export const likeButton = (id) => async dispatch =>{
  try {
    await axios.put(`/api/posts/like/${id}`)
    const res = await axios.get(`/api/posts/${id}`);

    dispatch({
      type:GET_POST,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
}

export const goingButton = (id) => async dispatch =>{
  try {
    await axios.put(`/api/posts/going/${id}`)
    const res = await axios.get(`/api/posts/${id}`);

    dispatch({
      type:GET_POST,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
}

export const getPosts = () => async dispatch =>{
  dispatch({type: CLEAR_POSTS})
  try {
    const res = await axios.get('/api/posts');

    dispatch({
      type:GET_POSTS,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const getPostScroll = (scroller,page,per) => async dispatch =>{
  try {
    dispatch({
      type:LOADING,
      payload:[]
    })
    const res = await axios.get(`/api/posts/pagedposts?page=${page}&limit=${per}`);
    const check = await axios.get('/api/posts');
    let feedback = [...scroller, ...res.data.result] 
    let nextpage = page+1;
    const limit = page*per;
    if(limit>check.data.length){
      nextpage = page;
      feedback = scroller;
      dispatch({
        type:SCROLL_END,
      })
    }
    dispatch({
      type:GET_SCROLL,
      payload: feedback
    });
    dispatch({
      type:PAGE_INCREMENT,
      payload:nextpage
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const refreshScroll = (scroller,page,per) => async dispatch =>{
  try {
    dispatch({
      type:LOADING,
      payload:[]
    })
    const pagination=page;
    // dispatch({
    //   type:CLEAR_SCROLL
    // });
    let feedback = [];
    for(let i=1; i<pagination; i++){
      const res = await axios.get(`/api/posts/pagedposts?page=${i}&limit=${per}`);
      feedback = [...feedback, ...res.data.result] 
    }
    let nextpage = pagination;
    dispatch({
      type:GET_SCROLL,
      payload: feedback
    });
    dispatch({
      type:PAGE_INCREMENT,
      payload:nextpage
    });
  } catch (err) {
    dispatch({
      type:POST_ERROR,
      payload:{msg:err.response.statusText, status: err.response.status}
    });
  }
};

export const loadingTrue = () => dispatch => {
  dispatch({
    type:LOADING,
    payload:[]
  })
}

export const getPost = id => async dispatch => {
  dispatch({
    type:LOADING,
    payload:[]
  })
  try {
    const res = await axios.get(`/api/posts/${id}`);

    dispatch({
      type: GET_POST,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const addComment = (postId, formData) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.post(
      `/api/posts/comment/${postId}`,
      formData,
      config
    );

    dispatch({
      type: ADD_COMMENT,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};
