import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getProfiles, getProfileById} from '../actions/profile';
import {getPosts, getPostScroll, filterPosts, getPost, resetFilter, changeEnd, changeStart, loadingTrue, refreshScroll} from '../actions/post';
import HeaderItem from './headerItem';
import Like from './like';
import Going from './going';
import { Link } from 'react-router-dom';
import { loadUser } from '../actions/auth';

class MainPost extends Component {
  async componentDidMount() {
    await this.props.loadUser()
    if(this.props.post.filtered===false){
      this.props.getPostScroll(this.props.post.scroller,  this.props.post.pagination,this.props.post.per);
    }
    this.props.getProfiles();
    window.addEventListener('scroll', this.handleScroll);
    const initDate = "2019-05-09";
    this.props.changeStart(initDate);
    this.props.changeEnd(initDate);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = (event) => {
    if (((window.innerHeight + window.scrollY+100) >= document.body.offsetHeight) && !this.props.post.loading && !this.props.post.end) {
      if(this.props.post.filtered === true){
        this.props.filterPosts(this.props.post.end, this.props.post.date, this.props.post.startDate, this.props.post.endDate, this.props.post.channel, this.props.post.scroller, this.props.post.pagination, this.props.post.per, this.props.post.filtered)
      }
      else {
        this.props.getPostScroll(this.props.post.scroller,  this.props.post.pagination,this.props.post.per);
      }
    }
  };
  
  updateComponent = async () =>{
    await this.props.refreshScroll(this.props.post.scroller,  this.props.post.pagination,this.props.post.per);
  }

  resetScroller = async () => {
    this.props.loadingTrue();
    this.props.resetFilter();
    this.props.getPostScroll(this.props.post.scroller,  this.props.post.pagination,this.props.post.per);
    this.props.getProfiles();
    window.addEventListener('scroll', this.handleScroll);
    const initDate = "2019-05-09";
    this.props.changeStart(initDate);
    this.props.changeEnd(initDate);
    let date = document.getElementsByName("dateFilter");
    for(let i=0;i<date.length;i++)
      date[i].checked = false;
    let channel = document.getElementsByName("channelFilter");
    for(let i=0;i<channel.length;i++)
      channel[i].checked = false;
    }

  render() { 
    const {
      post:{scroller, end, filtered, filteredSize, channel, startDate,endDate, date},
      profile:{profiles},
      getPost
    } = this.props;

    const changeFormat = (data) =>{
      const splitted = data.split("-") 
      return splitted[2]+"/"+splitted[1];
    }

    return ( 
      <>
        {scroller && profiles && filtered && 
          <div className="FilterDetails">
            <div className="Container">
              <div className="FilterHeader Clearfix">
                <div className="Size">{filteredSize} Results</div>
                <div className="ClearFilter" onClick={()=> this.resetScroller()}>
                  CLEAR SEARCH
                </div>
              </div>
              <div className="FilterDetail">
                Searched for {channel} Activities {!date ? '' : (date === 'later') ? `from ${changeFormat(startDate)} to ${changeFormat(endDate)}` : `from ${date}`}
              </div>
            </div>
          </div>
        }      
        <div className={filtered ? "Posts FilteredContainer" : "Posts"}>
          {scroller && profiles && scroller.map((post) => (
            <div className="Post" key={post._id}>
              <Link to='/event/details' onClick={()=>getPost(post._id)}>
                <div className="Header Clearfix">
                  {post && <HeaderItem singlePost={post}/>}
                  <div className="PostChannel">
                    {post.channel}
                  </div>
                </div>
                {
                  post.image.length===0 ?
                  <div className="PostDetailContainer Clearfix">
                    <div className="PostTitle">
                      {post.title}
                    </div>
                    <div className="Clearfix">
                      <svg className="TimeLogo" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path d="M21.2,21.6l-1.9,1.9l0,0l-5.6-5.5V7.6h2.7v9.2L21.2,21.6z M30.2,16.3c0,7.8-6.3,14.2-14.2,14.2 S1.8,24.2,1.8,16.3C1.8,8.5,8.2,2.2,16,2.1c0,0,0,0,0,0C23.8,2.2,30.2,8.5,30.2,16.3z M27.5,16.3C27.5,10,22.3,4.9,16,4.9 C9.7,4.9,4.5,10,4.5,16.3c0,6.3,5.1,11.5,11.5,11.5C22.3,27.8,27.5,22.7,27.5,16.3z"/></svg>
                      <div className="TimeText">
                        {post.start_date} {post.start} - {post.end_date} {post.end}
                      </div>
                    </div>
                    <div className="PostDetail">
                      {post.detail.length>150 ? post.detail.slice(0, 150) + '...' : post.detail }
                    </div>
                  </div>
                  :
                  <div className="PostDetailContainer Clearfix">
                    <div className="LeftDetail">
                      <div className="PostTitle">
                        {post.title}
                      </div>
                      <div className="Clearfix">
                        <svg className="TimeLogo" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path d="M21.2,21.6l-1.9,1.9l0,0l-5.6-5.5V7.6h2.7v9.2L21.2,21.6z M30.2,16.3c0,7.8-6.3,14.2-14.2,14.2 S1.8,24.2,1.8,16.3C1.8,8.5,8.2,2.2,16,2.1c0,0,0,0,0,0C23.8,2.2,30.2,8.5,30.2,16.3z M27.5,16.3C27.5,10,22.3,4.9,16,4.9 C9.7,4.9,4.5,10,4.5,16.3c0,6.3,5.1,11.5,11.5,11.5C22.3,27.8,27.5,22.7,27.5,16.3z"/></svg>
                        <div className="TimeText">
                          {post.start_date} - {post.end_date}
                        </div>
                      </div>
                      <div className="PostDetail">
                        {post.detail.length>150 ? post.detail.slice(0, 150) + '...' : post.detail }
                      </div>
                    </div>
                    <div className="ImageContainer">
                      <img src={post.image[0].imageUrl} alt={post.image[0].imageUrl} />
                    </div>
                  </div>
                }
              </Link>
              <div className="PostButton Clearfix">
                <Going list={post.going} PId={post._id} updateComponent={this.updateComponent} />
                <Like list={post.likes} PId={post._id} updateComponent={this.updateComponent} />
              </div>
            </div>
          ))}   
          {filteredSize === 0 && 
            <div className="Empty">
              <div className="Container">
                <svg className="LoadingIcon" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>no-activity</title><path d="M16,32A16,16,0,0,1,2.17,8h0a16.16,16.16,0,0,1,1.33-2h0A16,16,0,1,1,16,32ZM30,16a13.85,13.85,0,0,0-.16-1.85A13.5,13.5,0,0,0,28,14a13.86,13.86,0,0,0-6.23,1.52,24,24,0,0,1,4.64,9.75A13.89,13.89,0,0,0,30,16ZM24.7,26.91A22.32,22.32,0,0,0,20.06,16.5,14,14,0,0,0,14,28a13.6,13.6,0,0,0,.15,1.84A13.8,13.8,0,0,0,16,30,13.87,13.87,0,0,0,24.7,26.91ZM12.11,29.4c0-.46-0.09-0.93-0.09-1.4a16,16,0,0,1,6.72-13A22.78,22.78,0,0,0,17,13.33,16,16,0,0,1,4,20c-0.47,0-.92,0-1.38-0.09A14,14,0,0,0,12.11,29.4ZM4.47,8.09A13.92,13.92,0,0,0,2,16a13.77,13.77,0,0,0,.19,1.86A13.52,13.52,0,0,0,4,18a14,14,0,0,0,11.39-5.91A22.31,22.31,0,0,0,4.47,8.09Zm13.4-5.91A13.84,13.84,0,0,0,16,2,13.94,13.94,0,0,0,6,6.28a24.37,24.37,0,0,1,10.45,4.09A13.85,13.85,0,0,0,18,4,13.64,13.64,0,0,0,17.88,2.19Zm2.06,0.44C20,3.08,20,3.53,20,4a15.93,15.93,0,0,1-2,7.68,25.06,25.06,0,0,1,2.36,2.24A15.92,15.92,0,0,1,28,12c0.47,0,.94,0,1.4.09A14,14,0,0,0,19.93,2.62Z" transform="translate(-0.02 0)"/></svg>
                <div className="text">No activity Found</div>
              </div>
            </div>
          }
          {end && filteredSize !==0 &&
          <div className="Loading">
            <svg className="LoadingIcon" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>no-activity</title><path d="M16,32A16,16,0,0,1,2.17,8h0a16.16,16.16,0,0,1,1.33-2h0A16,16,0,1,1,16,32ZM30,16a13.85,13.85,0,0,0-.16-1.85A13.5,13.5,0,0,0,28,14a13.86,13.86,0,0,0-6.23,1.52,24,24,0,0,1,4.64,9.75A13.89,13.89,0,0,0,30,16ZM24.7,26.91A22.32,22.32,0,0,0,20.06,16.5,14,14,0,0,0,14,28a13.6,13.6,0,0,0,.15,1.84A13.8,13.8,0,0,0,16,30,13.87,13.87,0,0,0,24.7,26.91ZM12.11,29.4c0-.46-0.09-0.93-0.09-1.4a16,16,0,0,1,6.72-13A22.78,22.78,0,0,0,17,13.33,16,16,0,0,1,4,20c-0.47,0-.92,0-1.38-0.09A14,14,0,0,0,12.11,29.4ZM4.47,8.09A13.92,13.92,0,0,0,2,16a13.77,13.77,0,0,0,.19,1.86A13.52,13.52,0,0,0,4,18a14,14,0,0,0,11.39-5.91A22.31,22.31,0,0,0,4.47,8.09Zm13.4-5.91A13.84,13.84,0,0,0,16,2,13.94,13.94,0,0,0,6,6.28a24.37,24.37,0,0,1,10.45,4.09A13.85,13.85,0,0,0,18,4,13.64,13.64,0,0,0,17.88,2.19Zm2.06,0.44C20,3.08,20,3.53,20,4a15.93,15.93,0,0,1-2,7.68,25.06,25.06,0,0,1,2.36,2.24A15.92,15.92,0,0,1,28,12c0.47,0,.94,0,1.4.09A14,14,0,0,0,19.93,2.62Z" transform="translate(-0.02 0)"/></svg>
            <div className="text">No more post...</div>
          </div>
          }
        </div>
      </>
    );  
  }
}
 
MainPost.propTypes = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  getPosts: PropTypes.func.isRequired,
  getPostScroll: PropTypes.func.isRequired,
  getProfileById: PropTypes.func.isRequired,
  getProfiles: PropTypes.func.isRequired,
  filterPosts: PropTypes.func.isRequired,
  getPost:PropTypes.func.isRequired,
  loadUser:PropTypes.func.isRequired,
  resetFilter:PropTypes.func.isRequired,
  changeStart:PropTypes.func.isRequired,
  loadingTrue:PropTypes.func.isRequired,
  refreshScroll:PropTypes.func.isRequired,
  changeEnd:PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile : state.profile,
  auth : state.auth,
  post : state.post
});

const mapDispatchToProps={
  resetFilter, 
  loadUser, 
  getPost, 
  filterPosts, 
  getPosts,
  getProfiles,
  getPostScroll,
  getProfileById,
  changeStart,
  changeEnd,
  loadingTrue,
  refreshScroll
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPost);