import React, {Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {likeButton, getPost} from '../actions/post';

class Like extends Component {
  constructor(){
    super();
    this.state = {
      liked: false,
      likes: null
    };
    this.handleClick = this.handleClick.bind(this);
  } 

  async handleClick(id) {
    this.setState({
      liked: !this.state.liked
    });
    await this.props.likeButton(id);
    await this.props.getPost(id);
    if(this.props.updateComponent){
      this.props.updateComponent();
    }
  }

  componentWillUnmount(){
    getPost(this.props.PId);
    const currentPost = this.props.post.post
    this.setState({post : currentPost});
    this.setState({likes:this.props.list.length});
    this.setState({
      liked: false
    });    
    for (let i=0; i<this.props.list.length; i++){
      if (this.props.list[i].user===this.props.auth.user._id){
        this.setState({
          liked: true
        });    
        getPost(this.props.PId);
      } 
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.post.post !== prevProps.post.post) {
      this.setState({likes:this.props.post.post.likes.length})
    }
  }
  
  componentDidMount(){
    getPost(this.props.PId);
    const currentPost = this.props.post.post
    this.setState({post : currentPost});
    this.setState({likes:this.props.list.length});
    this.setState({
      liked: false
    });    
    for (let i=0; i<this.props.list.length; i++){
      if (this.props.list[i].user===this.props.auth.user._id){
        this.setState({
          liked: true
        });    
        getPost(this.props.PId);
      } 
    }
  }

  render() { 
    return (
      <Fragment>
        { this.state.liked === true ?
          <div onClick={()=> this.handleClick(this.props.PId)}>
            <svg className="ILikedButton" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>like-outline</title><path id="Shape" d="M22,4a8,8,0,0,0-6,2.79A8,8,0,0,0,10,4a7.26,7.26,0,0,0-7.33,7.33c0,5,4.53,9.15,11.4,15.39L16,28.47l1.93-1.76c6.87-6.23,11.4-10.33,11.4-15.37A7.26,7.26,0,0,0,22,4h0Z"/></svg>
            <div className="ActiveText">I like it</div>
          </div>:
          <div onClick={()=> this.handleClick(this.props.PId)}>
            <svg className="LikedButton" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path id="Shape_2_" d="M22,4c-2.3,0-4.5,1.1-6,2.8C14.5,5.1,12.3,4,10,4c-4.1,0-7.3,3.2-7.3,7.3c0,5,4.5,9.1,11.4,15.4l1.9,1.7l1.9-1.8c6.9-6.2,11.4-10.3,11.4-15.4C29.3,7.2,26.1,4,22,4L22,4z M16.1,24.7L16,24.9l-0.1-0.1C9.5,19,5.3,15.2,5.3,11.3c0-2.7,2-4.7,4.7-4.7c2.1,0,4.1,1.3,4.8,3.1h2.5C17.9,8,19.9,6.7,22,6.7c2.7,0,4.7,2,4.7,4.7C26.7,15.2,22.5,19,16.1,24.7L16.1,24.7z"/></svg>
            <div className="InactiveText">{this.state.likes} likes</div>
          </div>     
        }
      </Fragment>
    );
  }
}

Like.propTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  likeButton : PropTypes.func.isRequired,
  getPost : PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth : state.auth,
  post : state.post
});

const mapDispatchToProps ={
  likeButton, getPost
}
 
export default connect(mapStateToProps,mapDispatchToProps)(Like); 

