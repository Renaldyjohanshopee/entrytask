import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

const HeaderItem = ({
  singlePost,
  profile:{profiles},
  auth
}) => {
  let user;
  for (let i=0; i<profiles.length; i++){
    if (profiles[i]._id===singlePost.user){
      user = profiles[i];
    } 
  }

  return (  
    <Fragment>
      <div className="PostProfilePicture">
        <img src={user.avatar} alt={user.name} />
      </div>
      <div className="PostUsername">
        {user.username}
      </div>
    </Fragment>
  );
}

HeaderItem.propTypes = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  profile : state.profile,
  auth : state.auth,
});
 
export default connect(mapStateToProps)(HeaderItem); 

