import React from 'react';

const Input = ({name, label, classes, ...rest}) => {
    return ( 
      <input {...rest} id={name} name={name} className={classes} placeholder={label} />
    );
}
 
export default Input;