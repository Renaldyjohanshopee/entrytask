import React, { Component } from 'react';
import { withGoogleMap, GoogleMap } from 'react-google-maps';

class Map extends Component {

  render() {
   const GoogleMapExample = withGoogleMap(props => (
      <GoogleMap
        defaultCenter = { { lat: props.latitude, lng: props.longitude } }
        defaultZoom = { 4 }
      >
      </GoogleMap>
   ));
   return(
      <div>
        <GoogleMapExample
          containerElement={ <div style={{ height: `88px`, width: '100%' }} /> }
          mapElement={ <div style={{ height: `100%` }} /> }
          latitude={this.props.longitude}
          longitude = {this.props.latitude}
        />
      </div>
   );
   }
};
export default Map;
