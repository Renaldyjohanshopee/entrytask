import React from 'react';
import PropTypes from 'prop-types';
import {logout} from '../actions/auth';
import {connect} from 'react-redux';
import{closeDrawer, openDrawer} from '../actions/drawer';
import { Link } from 'react-router-dom';

const NavBar = ({
  auth:{user},
  drawer:{drawer},
  openDrawer,
  closeDrawer,
  logout ,
  classes
}) => {
  return ( 
    <div className="NavBar">
      {classes==="main" ?
        (<div className="SearchButton" onClick={drawer===false ? openDrawer : closeDrawer}>
          <svg className="SearchSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>search</title><path d="M20.67,18.67H19.61l-0.37-.36a8.68,8.68,0,1,0-.93.93l0.36,0.37v1.05l6.67,6.65,2-2Zm-8,0a6,6,0,1,1,6-6A6,6,0,0,1,12.67,18.67Z"/></svg>
        </div>)
        :
        (<Link to='/dashboard' className="SearchButton">
          <svg className="SearchSvg" id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M13.1,28.3v-8.7h5.8v8.7h7.2V16.7h4.4L16,3.7L1.5,16.7h4.3v11.6H13.1z"/></svg>
        </Link>)
      }
      <div className="NavBarLogo">
        <svg className="LogoSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>logo-cat</title><polygon points="26.47 14.44 23.07 19.93 23.07 27.38 25.83 29.84 19.2 29.84 21.89 27.36 21.89 19.72 15.69 10.95 19.62 10.95 21.48 9.19 18.18 4.17 14.73 3.14 15.15 -0.03 9.92 4.17 2.83 17.38 7.78 28.12 5.51 30.53 5.51 31.97 9.26 31.97 10.18 31.48 10.93 31.97 29.94 31.97 29.94 30.25 25.68 25.99 25.68 20.55 27.96 16.84 28.78 16.84 29.2 20.08 30.4 20.08 30.71 14.44 26.47 14.44"/></svg>
      </div>
      <Link to='/profile/likes'>
        <div className="UserProfileNav">
          <img className="ProfilePicture" src={user && user.avatar} alt={user && user.username}/>
        </div>
      </Link>
    </div>
   );
};

NavBar.propTypes = {
  logout:PropTypes.func.isRequired,
  auth:PropTypes.object.isRequired,
  drawer:PropTypes.object.isRequired,
  closeDrawer:PropTypes.func.isRequired,
  openDrawer:PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth : state.auth,
  drawer : state.drawer
});

const mapDispatchToProps ={
  logout, closeDrawer, openDrawer
}
 
export default connect(mapStateToProps, mapDispatchToProps )(NavBar);