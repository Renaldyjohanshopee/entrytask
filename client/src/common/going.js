import React, {Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {goingButton, getPost} from '../actions/post';

class Going extends Component {
  constructor(){
    super();
    this.state = {
      going: false,
      goings: null
    };
    this.handleClick = this.handleClick.bind(this);
  } 

  async handleClick(id) {
    this.setState({
      going: !this.state.going
    });
    await this.props.goingButton(id);
    await this.props.getPost(id);
    if(this.props.updateComponent){
      this.props.updateComponent();
    }
  }

  componentWillUnmount(){
    getPost(this.props.PId);
    const currentPost = this.props.post.post
    this.setState({post : currentPost});
    this.setState({goings:this.props.list.length});
    this.setState({
      going: false
    });    
    for (let i=0; i<this.props.list.length; i++){
      if (this.props.list[i].user===this.props.auth.user._id){
        this.setState({
          going: true
        });    
        getPost(this.props.PId);
      } 
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.post.post !== prevProps.post.post) {
      this.setState({goings:this.props.post.post.going.length})
    }
  }
  
  componentDidMount(){
    getPost(this.props.PId);
    const currentPost = this.props.post.post
    this.setState({post : currentPost});
    this.setState({goings:this.props.list.length});
    this.setState({
      going: false
    });    
    for (let i=0; i<this.props.list.length; i++){
      if (this.props.list[i].user===this.props.auth.user._id){
        this.setState({
          going: true
        });    
        getPost(this.props.PId);
      } 
    }
  }

  render() { 
    return (
      <Fragment>
        { this.state.going === true ?
          <div onClick={()=>this.handleClick(this.props.PId)}>
            <svg className="IamGoingButton" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06Z"/></svg>          
            <div className="ActiveText">I am going!</div>
          </div>:
          <div onClick={()=>this.handleClick(this.props.PId)}>
          <svg className="GoingButton" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check-outline</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06ZM25.63,5.89L12,20.14a1,1,0,0,1-1.43,0L5.68,15.64,2.81,18.44l9.14,8.66L29.21,9.38Z"/></svg>        
            <div className="InactiveText">{this.state.goings} going</div>
          </div>
        }
      </Fragment>
    );
  }
}

Going.propTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  goingButton : PropTypes.func.isRequired,
  getPost : PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth : state.auth,
  post : state.post
});

const mapDispatchToProps={goingButton, getPost}
 
export default connect(mapStateToProps,mapDispatchToProps)(Going); 

