import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

const Publisher = ({
  singlePost,
  profile:{profiles},
}) => {
  let user;
  for (let i=0; i<profiles.length; i++){
    if (profiles[i]._id===singlePost.user){
      user = profiles[i];
    } 
  }
  const getBetween = date =>{
    const EndDate = new Date();
    const startDate = new Date(date);
    const oneDay = 1000 * 60 * 60 * 24;
  
    const start = Date.UTC(EndDate.getFullYear(), EndDate.getMonth(), EndDate.getDate());
    const end = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
  
    return (start - end) / oneDay;
  }
    

  return (  
    <Fragment>
      <div className="ProfilePicture">
        <img src={user.avatar} alt={user.name} />
      </div>
      <div className="Container">
        <div className="Username">
          {user.username}
        </div>
        <div className="PostTime">
          Published {getBetween(singlePost.published)} days ago
        </div>
      </div>
    </Fragment>
  );
}

Publisher.propTypes = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  profile : state.profile,
  auth : state.auth,
});
 
export default connect(mapStateToProps)(Publisher); 

