import React, { useState, useEffect } from 'react';
import NavBar from '../common/navbar';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Publisher from '../common/publisher';
import EventDetailContainer from './eventDetail';
import { Switch } from 'react-router';
import ProtectedRoute from '../common/protectedRoutes';
import Comment from './comment';
import { Link } from 'react-router-dom';
import Participant from './participant';
import {likeButton, getPost, goingButton, addComment} from '../actions/post';

const Event = ({
  post:{post, loading},
  auth:{user},
  likeButton,
  goingButton,
  getPost,
  addComment
  }) => {
  const [activeButton, setActiveButton] = useState(0);
  const [text, setText] = useState('');

  useEffect(()=>{
    setActiveButton('details');
  }, []);

  const onSubmit= async e=>{
    if (e!==null)
      await addComment(post._id, {text});
      setText('');
      await getPost(post._id);
    }

  const checkLike = () =>{
    for (let i=0; i<post.likes.length; i++){
      if (post.likes[i].user===user._id){
        return true;
      } 
    }
    return false;
  }

  const checkGoing = () =>{
    for (let i=0; i<post.going.length; i++){
      if (post.going[i].user===user._id){
        return true;
      } 
    }
    return false;
  }

  const clickGoing= async (id) => {
    await goingButton(id);
    checkGoing();
  }

  const clickLike= async (id) => {
    await likeButton(id);
    checkLike();
  }

  return ( 
    <>
      <NavBar classes="Event"/>
      {post && !loading ?  
        <>     
          <div className="EventContainer">
            {
            activeButton!=='comment' ?             
            <>
              <div className="PostChannel">
                {post.channel}
              </div>
              <div className="PostTitle">
                {post.title}
              </div>
              <div className="PublisherProfile Clearfix" >
                <Publisher singlePost={post}/>
              </div>
            </> :
            <>
            </>
            }
            <div className={ activeButton !=='comment' ? "OptionContainer" : "OptionContainer FixOption"}>
              <Link to='/event/details' onClick={()=> setActiveButton('details')}>
                <div className="Option">
                  {
                    activeButton==='details' ? 
                    <div className="TextContainer ActiveState">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>info</title><path d="M16,2.67A13.33,13.33,0,1,0,29.33,16,13.34,13.34,0,0,0,16,2.67Zm1.33,20H14.67v-8h2.67v8Zm0-10.67H14.67V9.33h2.67V12Z"/></svg>                
                      <div className="Text">
                        details
                      </div>
                    </div>:
                    <div className="TextContainer">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>info-outline</title><path d="M14.67,22.67h2.67v-8H14.67v8ZM16,2.67A13.33,13.33,0,1,0,29.33,16,13.34,13.34,0,0,0,16,2.67Zm0,24A10.67,10.67,0,1,1,26.67,16,10.68,10.68,0,0,1,16,26.67ZM14.67,12h2.67V9.33H14.67V12Z"/></svg>                  
                      <div className="Text">
                        details
                      </div>
                    </div>
                  }
                </div>
              </Link>
              <Link to='/event/participant' onClick={()=> setActiveButton('participant')}>
                <div className="Option Mid">
                  {
                    activeButton==='participant' ? 
                    <div className="TextContainer ActiveState">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>people</title><path d="M21.33,14.67a4,4,0,1,0-4-4A4,4,0,0,0,21.33,14.67Zm-10.67,0a4,4,0,1,0-4-4A4,4,0,0,0,10.67,14.67Zm0,2.67c-3.11,0-9.33,1.56-9.33,4.67v3.33H20V22C20,18.89,13.77,17.33,10.67,17.33Zm10.67,0c-0.39,0-.83,0-1.29.07A5.63,5.63,0,0,1,22.67,22v3.33h8V22C30.67,18.89,24.44,17.33,21.33,17.33Z"/></svg>
                      <div className="Text">
                        participant
                      </div>
                    </div>:
                    <div className="TextContainer">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>people-outline</title><path d="M22,17.33a16,16,0,0,0-6,1.33,15.73,15.73,0,0,0-6-1.33c-2.89,0-8.67,1.44-8.67,4.33v3.67H30.67V21.67C30.67,18.77,24.89,17.33,22,17.33Zm-5.33,6H3.33V21.67c0-.72,3.41-2.33,6.67-2.33s6.67,1.61,6.67,2.33v1.67Zm12,0h-10V21.67A2.43,2.43,0,0,0,18,20a12.86,12.86,0,0,1,4-.71c3.25,0,6.67,1.61,6.67,2.33v1.67ZM10,16a4.67,4.67,0,1,0-4.67-4.67A4.67,4.67,0,0,0,10,16Zm0-7.33a2.67,2.67,0,1,1-2.67,2.67A2.67,2.67,0,0,1,10,8.67ZM22,16a4.67,4.67,0,1,0-4.67-4.67A4.67,4.67,0,0,0,22,16Zm0-7.33a2.67,2.67,0,1,1-2.67,2.67A2.67,2.67,0,0,1,22,8.67Z"/></svg>
                      <div className="Text">
                        participant
                      </div>
                    </div>  
                  }                 
                </div>
              </Link>
              <Link to='/event/comment' onClick={()=> setActiveButton('comment')}>
                <div className="Option">
                  {
                    activeButton==='comment' ? 
                    <div className="TextContainer ActiveState">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>chat</title><path d="M30.9,16a10.9,10.9,0,0,1-4.53,8.71v6.51l-5.87-4.11a12.1,12.1,0,0,1-1.75.15A12.77,12.77,0,0,1,12,25.37l-1.15-.72a15.43,15.43,0,0,0,1.65.1,15.42,15.42,0,0,0,1.6-.09c6.92-.72,12.32-6,12.32-12.5a11.41,11.41,0,0,0-.13-1.62,11.67,11.67,0,0,0-1.53-4.33l0.59,0.35A11.05,11.05,0,0,1,30.9,16Zm-6.21-4c0-6-5.3-10.93-11.82-10.93-0.25,0-.5,0-0.75,0C6,1.47,1.06,6.21,1.06,12a10.44,10.44,0,0,0,3.7,7.91v5.86l2.69-1.89,2.07-1.45a12.38,12.38,0,0,0,3.36.5C19.39,22.94,24.69,18,24.69,12Z"/></svg>                  
                      <div className="Text">
                        comment
                      </div>
                    </div>:
                    <div className="TextContainer">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>comment</title><path d="M30.9,16a10.9,10.9,0,0,1-4.53,8.71v6.51l-5.87-4.11a12.1,12.1,0,0,1-1.75.15A12.77,12.77,0,0,1,12,25.37l-1.21-.76,1.66,0.08a14,14,0,0,0,3.66-.33l0.08,0,0.08,0a9.78,9.78,0,0,0,4.5.14l0.14,0,2.92,1.75V23.37L24,23.26A8.48,8.48,0,0,0,28.34,16a8.1,8.1,0,0,0-2-5.24l-0.08-.16a10.82,10.82,0,0,0-1.47-3.52L24,5.75l1.37,0.82A11.05,11.05,0,0,1,30.9,16Zm-18,6.92a12.38,12.38,0,0,1-3.36-.5L4.76,25.78V19.91A10.44,10.44,0,0,1,1.06,12c0-6,5.3-10.93,11.82-10.93S24.69,6,24.69,12,19.39,22.94,12.88,22.94ZM9.11,19.69l0.17,0.07a10.09,10.09,0,0,0,3.6.66c5.13,0,9.31-3.78,9.31-8.42S18,3.57,12.88,3.57,3.57,7.35,3.57,12a8.11,8.11,0,0,0,3.6,6.6l0.16,0.11v2.05Z"/></svg>
                      <div className="Text">
                        comment
                      </div>
                    </div>
                  } 
                </div>
              </Link>
            </div>
            <Switch>
              <ProtectedRoute exact path='/event/details'>
                <EventDetailContainer post={post} clickHandler={setActiveButton} replyHandler={setText} />
              </ProtectedRoute>
              <ProtectedRoute path='/event/comment'>
                <Comment post={post} click={setActiveButton} reply={setText}/>
              </ProtectedRoute>
              <ProtectedRoute path='/event/participant'>
                <Participant post={post} clickHandler={setActiveButton} replyHandler={setText}/>
              </ProtectedRoute>
            </Switch>
          </div>
          <div className="BottomNav Clearfix">
          {
            activeButton!=="comment" ?
            <>
              <div className="Purple">
                <div className="Comment">
                  <Link to='/event/comment'>
                    <svg onClick={()=> setActiveButton('comment')} className="CommentSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>comment-single</title><path d="M18.31,14.19A2.31,2.31,0,1,1,16,11.88,2.31,2.31,0,0,1,18.31,14.19ZM8.76,11.88a2.31,2.31,0,1,0,2.31,2.31A2.31,2.31,0,0,0,8.76,11.88Zm14.49,0a2.31,2.31,0,1,0,2.31,2.31A2.31,2.31,0,0,0,23.25,11.88ZM31,14.19c0,7.27-6.72,13.18-15,13.18-0.49,0-1,0-1.54-.08l-8,4v-7A12.55,12.55,0,0,1,1,14.19C1,6.92,7.73,1,16,1S31,6.92,31,14.19Zm-2.81,0C28.18,8.47,22.72,3.82,16,3.82S3.82,8.47,3.82,14.19A10,10,0,0,0,9,22.62l0.24,0.15V27l4.5-2.25a0.37,0.37,0,0,1,.18-0.09l0,0h0l0.22,0v0H16C22.72,24.56,28.18,19.91,28.18,14.19Z"/></svg>
                  </Link>
                </div>
                <div className="Like">
                  {checkLike() ? 
                  <svg onClick={()=>clickLike(post._id)} className="Active" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>like-outline</title><path id="Shape" d="M22,4a8,8,0,0,0-6,2.79A8,8,0,0,0,10,4a7.26,7.26,0,0,0-7.33,7.33c0,5,4.53,9.15,11.4,15.39L16,28.47l1.93-1.76c6.87-6.23,11.4-10.33,11.4-15.37A7.26,7.26,0,0,0,22,4h0Z"/></svg>
                  :
                  <svg onClick={()=>clickLike(post._id)} className="Inactive" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path id="Shape_2_" d="M22,4c-2.3,0-4.5,1.1-6,2.8C14.5,5.1,12.3,4,10,4c-4.1,0-7.3,3.2-7.3,7.3c0,5,4.5,9.1,11.4,15.4l1.9,1.7l1.9-1.8c6.9-6.2,11.4-10.3,11.4-15.4C29.3,7.2,26.1,4,22,4L22,4z M16.1,24.7L16,24.9l-0.1-0.1C9.5,19,5.3,15.2,5.3,11.3c0-2.7,2-4.7,4.7-4.7c2.1,0,4.1,1.3,4.8,3.1h2.5C17.9,8,19.9,6.7,22,6.7c2.7,0,4.7,2,4.7,4.7C26.7,15.2,22.5,19,16.1,24.7L16.1,24.7z"/></svg>
                  }
                </div>
              </div>
              <div className="Green">
                <div className="Container">
                  <div className="Going" onClick={()=>clickGoing(post._id)} >
                  {checkGoing() ? 
                    <>
                      <svg className="Active" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06Z"/></svg>          
                      <div className="Text Active">I am going</div>                  
                    </>
                  :
                    <>
                      <svg className="Inactive" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check-outline</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06ZM25.63,5.89L12,20.14a1,1,0,0,1-1.43,0L5.68,15.64,2.81,18.44l9.14,8.66L29.21,9.38Z"/></svg>        
                      <div className="Text">Join</div>                  
                    </>
                  }
                  </div>
                </div>
              </div>
            </>
            :
            <>
              <div className="PurpleComment">
                <div className="Cross">
                  <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>cross</title><polygon points="30 5.33 27.17 2.5 16 13.67 4.83 2.5 2 5.33 13.17 16.5 2 27.67 4.83 30.5 16 19.33 27.17 30.5 30 27.67 18.83 16.5 30 5.33"/></svg>
                </div>
                <div className="InputComment">
                  <div className="InputContainer">
                    <input 
                      name='text'
                      value={text}
                      required
                      placeholder="Leave your comment here" 
                      onChange={e => setText(e.target.value)} />
                  </div>
                </div>
              </div>
              <div className="GreenComment" onClick={()=>onSubmit(text)}>
                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>send</title><path d="M2.68,28l28-12L2.68,4v9.33l20,2.67-20,2.67V28Z"/></svg>
              </div>
            </>
          }
          </div>  
        </>    
        :
        <div className="EventLoad"></div>
      }
      
    </>
  );
}
 
Event.propTypes = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  likeButton : PropTypes.func.isRequired,
  goingButton : PropTypes.func.isRequired,
  getPost : PropTypes.func.isRequired,
  addComment : PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile : state.profile,
  auth : state.auth,
  post : state.post
});

const mapDispatchToProps = {
  addComment, likeButton, goingButton,getPost
}
 
export default connect(mapStateToProps,mapDispatchToProps)(Event);
