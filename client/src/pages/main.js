import React, { Fragment } from 'react';
import NavBar from '../common/navbar';
import MainPost from '../common/post';
import SlideDrawer from './slideDrawer';
import Backdrop from './backdrop';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const Main = ({
  drawer:{drawer},
}) => {
  let backdrop = drawer === true ? <Backdrop/>:<Fragment/>;

  return ( 
  <>
    <SlideDrawer/>
    {backdrop}
    <div className={ drawer===false ? "Main" : "Main Slide"} >
      <NavBar classes="main"/>
      <div className="MainFeed">
      <MainPost/>
      </div>
    </div> 
  </>)
  ;
}
 
Main.propTypes = {
  drawer: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  drawer : state.drawer,
});
 
export default connect(mapStateToProps)(Main);
