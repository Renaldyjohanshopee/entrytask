import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {getPosts, changeChannel, changeDate,filterPosts, clearScroll, filterTrue, changeEnd, changeStart } from '../actions/post';

class SlideDrawer extends Component {
  async handleClick() {
    await this.props.clearScroll();
    this.props.filterPosts(this.props.post.end, this.props.post.date, this.props.post.startDate, this.props.post.endDate, this.props.post.channel, this.props.post.scroller, this.props.post.pagination, this.props.post.per, this.props.post.filtered);
    this.props.filterTrue();
  }

  handleOnChangeDate(e) {
    this.props.changeDate(e.target.value);
  }

  handleOnChangeChannel(e) {
    this.props.changeChannel(e.target.value);
  }

  handleOnChangeStart(e){
    this.props.changeStart(e.target.value);
  }

  handleOnChangeEnd(e){
    this.props.changeEnd(e.target.value);
  }

  render() { 
    const {
      drawer:{drawer},
      post:{date,channel}
    } = this.props;
    return (  
      <div className={drawer===true ? "SideDrawer Open" : "SideDrawer"}>
        <div className="Title">
          <div className="TitleText">
            DATE
          </div>
        </div>
        <div className="Date FilterContainer Clearfix">
          <input type="radio" value="anytime" onChange={(e)=> this.handleOnChangeDate(e)} name="dateFilter" id="anytime" />
          <label className="FilterInput" htmlFor="anytime">ANYTIME</label>            
          <input type="radio" value="today" onChange={(e)=> this.handleOnChangeDate(e)} name="dateFilter" id="today" />
          <label className="FilterInput" htmlFor="today">TODAY</label>            
          <input type="radio" value="tomorrow" onChange={(e)=> this.handleOnChangeDate(e)} name="dateFilter" id="tomorrow" />
          <label className="FilterInput" htmlFor="tomorrow">TOMORROW</label>            
          <input type="radio" value="this week" onChange={(e)=> this.handleOnChangeDate(e)} name="dateFilter" id="thisweek" />
          <label className="FilterInput" htmlFor="thisweek">THIS WEEK</label>            
          <input type="radio" value="this month" onChange={(e)=> this.handleOnChangeDate(e)} name="dateFilter" id="thismonth" />
          <label className="FilterInput" htmlFor="thismonth">THIS MONTH</label>            
          <input type="radio" value="later" onChange={(e)=> this.handleOnChangeDate(e)} name="dateFilter" id="later" />
          <label className="FilterInput" htmlFor="later">LATER</label>            
        </div>
        {date === "later" ?
            <div className="LaterOption">
              <div className="Tri"/>
              <div className="InputContainer Clearfix">
                <svg className="DateSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>date2</title><path d="M0,2.91H4V29.09H0V2.91ZM6.33,14h0v4H22.68l-4.14,4.09,2.81,2.85L30.4,16,21.35,7.06,18.54,9.91,22.68,14H6.33Z"/></svg>                
                <div className="Container">
                  <input type="date" className="InputDate" onChange={(e)=>this.handleOnChangeStart(e)} defaultValue="09/05/2019" />
                </div>
                <div className="HrContainer">
                  <hr />
                </div>
                <svg className="DateSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>date</title><path d="M32,29.09H28V2.91h4V29.09ZM9.32,14l4.14-4.09L10.65,7.06,1.6,16l9.05,8.94,2.81-2.85L9.32,18H25.67V14H9.32Z"/></svg>
                <div className="Container">
                  <input type="date" className="InputDate" onChange={(e)=>this.handleOnChangeEnd(e)} defaultValue="09/05/2019" />
                </div>
              </div> 
            </div>: null
        }
        <div className="Title">
          <div className="TitleText">
            CHANNEL
          </div>
        </div>
        <div className="Channel FilterContainer Clearfix">
          <input type="radio" value="All channel" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="all" />
          <label className="FilterInput" htmlFor="all">All</label>            
          <input type="radio" value="Channel 1" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="channel1" />
          <label className="FilterInput" htmlFor="channel1">Channel 1</label>            
          <input type="radio" value="Channel 2" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="channel2" />
          <label className="FilterInput" htmlFor="channel2">Channel 2</label>            
          <input type="radio" value="Channel 3" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="channel3" />
          <label className="FilterInput" htmlFor="channel3">Channel 3</label>            
          <input type="radio" value="Channel 4" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="channel4" />
          <label className="FilterInput" htmlFor="channel4">Channel 4</label>            
          <input type="radio" value="Channel 5" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="channel5" />
          <label className="FilterInput" htmlFor="channel5">Channel 5</label>            
          <input type="radio" value="Channel 6" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="channel6" />
          <label className="FilterInput" htmlFor="channel6">Channel 6</label>            
          <input type="radio" value="Short" onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="short" />
          <label className="FilterInput" htmlFor="short">Short</label>            
          <input type="radio" value="Channel Long Name " onChange={(e)=> this.handleOnChangeChannel(e)} name="channelFilter" id="long" />
          <label className="FilterInput" htmlFor="long">Channel Long Name</label>            
        </div>
        { channel || date ?
          <div className="FilterSubmit" id="filtersubmit">
            <div className="TextContainer">
              <svg className="SearchSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>search</title><path d="M20.67,18.67H19.61l-0.37-.36a8.68,8.68,0,1,0-.93.93l0.36,0.37v1.05l6.67,6.65,2-2Zm-8,0a6,6,0,1,1,6-6A6,6,0,0,1,12.67,18.67Z"/></svg>
              <div className= "Text" onClick={()=> this.handleClick()}>
                SEARCH
              </div>
            </div>
            <div className="FilterText">
              {channel} activities {date ? "from" : null} {date}
            </div>
          </div>
        :
          <div className="FilterSubmitInactive" id="filtersubmit">
            <div className="TextContainer">
              <svg className="SearchSvg" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>search</title><path d="M20.67,18.67H19.61l-0.37-.36a8.68,8.68,0,1,0-.93.93l0.36,0.37v1.05l6.67,6.65,2-2Zm-8,0a6,6,0,1,1,6-6A6,6,0,0,1,12.67,18.67Z"/></svg>
              <div className= "Text">
                SEARCH
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}
 
SlideDrawer.propTypes = {
  drawer: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  drawer : state.drawer,
  post : state.post
});

const mapDispatchToProps = {
  getPosts,
  changeDate,
  changeChannel,
  filterPosts,
  clearScroll,
  filterTrue,
  changeStart,
  changeEnd
}

export default connect(mapStateToProps, mapDispatchToProps)(SlideDrawer);
