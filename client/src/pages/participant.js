import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Comment from '../common/comment';

const Participant = ({
  post,
  profile:{profiles},
  clickHandler,
  replyHandler
}) => {

  const getProfilePicture = (id) => {
    for (let i=0; i<profiles.length; i++){
      if (profiles[i]._id===id){
        return profiles[i].avatar
      } 
    }
  }

  return (  
    <div className="Participant">
      <div className="GoingContainer Clearfix">
        <div className="GoingCounter Clearfix">
          <svg className="Check" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check-outline</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06ZM25.63,5.89L12,20.14a1,1,0,0,1-1.43,0L5.68,15.64,2.81,18.44l9.14,8.66L29.21,9.38Z"/></svg>        
          <div className="InactiveText">{post.going.length} going</div>
        </div>
        <div className="ProfileContainer Clearfix">
          {post.going.map(going => <img src={getProfilePicture(going.user)} key={going.user} alt={going.user} />)}
        </div>
      </div>
      <div className="LikeContainer Clearfix">
        <div className="LikeCounter Clearfix">
          <svg className="Like" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path id="Shape_2_" d="M22,4c-2.3,0-4.5,1.1-6,2.8C14.5,5.1,12.3,4,10,4c-4.1,0-7.3,3.2-7.3,7.3c0,5,4.5,9.1,11.4,15.4l1.9,1.7l1.9-1.8c6.9-6.2,11.4-10.3,11.4-15.4C29.3,7.2,26.1,4,22,4L22,4z M16.1,24.7L16,24.9l-0.1-0.1C9.5,19,5.3,15.2,5.3,11.3c0-2.7,2-4.7,4.7-4.7c2.1,0,4.1,1.3,4.8,3.1h2.5C17.9,8,19.9,6.7,22,6.7c2.7,0,4.7,2,4.7,4.7C26.7,15.2,22.5,19,16.1,24.7L16.1,24.7z"/></svg>
          <div className="InactiveText">{post.likes.length} likes</div>
        </div>
        <div className="ProfileContainer Clearfix">
          {post.likes.map(likes => <img src={getProfilePicture(likes.user)} key={likes.user} alt={likes.user} />)}
        </div>
      </div>
      <div className="CommentContainer">
        <Comment click={clickHandler} reply={replyHandler}/>
      </div>
    </div>
  );
}

Participant.propTypes = {
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile : state.profile
});
 
export default connect(mapStateToProps)(Participant);
