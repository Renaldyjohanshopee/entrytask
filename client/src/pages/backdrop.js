import React from 'react';
import {connect} from 'react-redux';
import { closeDrawer } from '../actions/drawer';
import PropTypes from 'prop-types';

const Backdrop = ({
  closeDrawer
}) => {
  return (  
    <div className="Backdrop" onClick={closeDrawer}/>
  );
}

Backdrop.propTypes = {
  closeDrawer: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  drawer : state.drawer
});
 
export default connect(mapStateToProps, {closeDrawer})(Backdrop);
