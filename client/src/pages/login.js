import React, { useState } from 'react';
import background from './../images/loginBackground.jpg';
import Input from './../common/input';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import {login} from '../actions/auth';
import { Redirect } from 'react-router';

const Login = ({login, isAuthenticated}) => {
  const [formData, setFormData] = useState({
    username:'',
    password: ''
  });

  const {username, password} = formData;

  const onChange = e =>
    setFormData({...formData, [e.target.name]:e.target.value});

  const onSubmit=async e=>{
    e.preventDefault();
    login(username, password);
  }

  if(isAuthenticated){
    return <Redirect to="/dashboard"/>
  }

  return (
    <div className="Login">
      <form onSubmit={e => onSubmit(e)}>
        <div className="Container">
          <div className="LoginTitle">
            FIND THE MOST LOVED ACTIVITIES
          </div>
          <div className="LoginSubTitle">
            BLACK CAT
          </div>
          <div className="LoginLogo">
            <svg className="LogoImage" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>logo-cat</title><polygon points="26.47 14.44 23.07 19.93 23.07 27.38 25.83 29.84 19.2 29.84 21.89 27.36 21.89 19.72 15.69 10.95 19.62 10.95 21.48 9.19 18.18 4.17 14.73 3.14 15.15 -0.03 9.92 4.17 2.83 17.38 7.78 28.12 5.51 30.53 5.51 31.97 9.26 31.97 10.18 31.48 10.93 31.97 29.94 31.97 29.94 30.25 25.68 25.99 25.68 20.55 27.96 16.84 28.78 16.84 29.2 20.08 30.4 20.08 30.71 14.44 26.47 14.44"/></svg>                
          </div>
          <img src={background} className="BackImage" alt="background" />    
          <div className="Overlay" />
          <div className="Username">
            <svg className="InputLogo" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>user</title><path d="M16,1.94A14.06,14.06,0,1,0,30.06,16,14.06,14.06,0,0,0,16,1.94Zm0,4.22a4.22,4.22,0,1,1-4.22,4.22A4.21,4.21,0,0,1,16,6.16Zm0,20a10.12,10.12,0,0,1-8.43-4.53c0-2.8,5.62-4.33,8.43-4.33s8.39,1.53,8.43,4.33A10.12,10.12,0,0,1,16,26.12Z"/></svg>
            <Input 
              id="username" 
              name="username" 
              label="Username" 
              classes="LoginInput" 
              value={username} 
              onChange={e=>onChange(e)} />
          </div>
          <div className="Password">
            <svg className="InputLogo" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>password</title><path d="M13.33,30.67h5.33V28H24V22.67H18.67v-5.8a8,8,0,1,0-5.33,0v13.8ZM16,12a2.67,2.67,0,1,1,2.67-2.67A2.67,2.67,0,0,1,16,12Z"/></svg>
            <Input 
              id="password" 
              name="password" 
              label="Password" 
              classes="LoginInput" 
              type="password"
              value={password}
              onChange={e=>onChange(e)} />
          </div>
        </div>
        <input type="submit" className="EnterButton" id="signin" value="SIGN IN"/>
      </form>
    </div>
  );
}

Login.prototype = {
  login:PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  isAuthenticated:state.auth.isAuthenticated
});

export default connect(mapStateToProps,{login})(Login);