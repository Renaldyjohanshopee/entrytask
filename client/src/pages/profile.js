import React, { Component } from 'react';
import NavBar from '../common/navbar';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getPosts, getPost} from '../actions/post';
import HeaderItem from '../common/headerItem';
import Going from '../common/going';
import Like from '../common/like';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      likes : null,
      going : null,
      past : null,
      activeButton:'likes',
      activePosts:[]
    };
  }
  
  async componentDidMount(){
    await this.props.getPosts();
    this.setState({
      likes:this.countLike(),
      going:this.countGoing(),
      past:this.countPast(),
      activePosts:this.filterLikes()
    })
  }

  updateComponent = async () =>{
    await this.props.getPosts();
    this.setState({
      likes:this.countLike(),
      going:this.countGoing(),
      past:this.countPast(),
    })
  }

  countLike = () =>{
    let count=0;
    for (let i=0; i<this.props.post.posts.length; i++){
      for (let j=0; j<this.props.post.posts[i].likes.length; j++){
        if (this.props.post.posts[i].likes[j].user===this.props.auth.user._id){
          count = count+1;
        } 
      }
    }
    return count;
  }
  
  countGoing = () =>{
    let count=0;
    for (let i=0; i<this.props.post.posts.length; i++){
      for (let j=0; j<this.props.post.posts[i].going.length; j++){
        if (this.props.post.posts[i].going[j].user===this.props.auth.user._id){
          count = count+1;
        } 
      }
    }
    return count;
  }

  countPast = () =>{
    let count=0;
    for (let i=0; i<this.props.post.posts.length; i++){
      for (let j=0; j<this.props.post.posts[i].going.length; j++){
        if (this.props.post.posts[i].going[j].user===this.props.auth.user._id){
          const event = new Date(this.props.post.posts[i].event_end);
          const today = new Date();
          if(today>event){
            count = count+1;
          }
        } 
      }
    }
    return count;
  }

  filterLikes = () =>{
    let filteredPost = []
    for (let i=0; i<this.props.post.posts.length; i++){
      for (let j=0; j<this.props.post.posts[i].likes.length; j++){
        if (this.props.post.posts[i].likes[j].user===this.props.auth.user._id){
          filteredPost.push(this.props.post.posts[i])
        } 
      }
    }
    return filteredPost;
  }

  filterGoing = () =>{
    let filteredPost = []
    for (let i=0; i<this.props.post.posts.length; i++){
      for (let j=0; j<this.props.post.posts[i].going.length; j++){
        if (this.props.post.posts[i].going[j].user===this.props.auth.user._id){
          const event = new Date(this.props.post.posts[i].event_end);
          const today = new Date();
          if(today>event){
            filteredPost.push(this.props.post.posts[i])
          }
        } 
      }
    }
    return filteredPost;
  }

  filterPast = () =>{
    let filteredPost = []
    for (let i=0; i<this.props.post.posts.length; i++){
      for (let j=0; j<this.props.post.posts[i].going.length; j++){
        if (this.props.post.posts[i].going[j].user===this.props.auth.user._id){
          filteredPost.push(this.props.post.posts[i])
        } 
      }
    }
    return filteredPost;
  }

  handleButton = (e) =>{
    this.setState({activeButton:e})
    if (e==='likes'){
      this.setState({activePosts:this.filterLikes()})
    }else if (e==='going'){
      this.setState({activePosts:this.filterGoing()})
    }else{
      this.setState({activePosts:this.filterPast()})
    }
  }

  render() { 
    const {
      auth:{user},
    } = this.props;
    
    return (
      <>
        <NavBar classes="Event"/>
        <div className="Details">
          <div className="PictureContainer">
            <div className="Picture">
              <img src={user.avatar} alt={user.username} />
            </div>
          </div>
          <div className="Username">
            <div className="Text">
              {user.username}
            </div>
          </div>
          <div className="Email">
            <div className="Container">
              <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>email</title><path d="M30.33,8.36a2.68,2.68,0,0,0,0-.32l0-.09L29.77,8h0l0.49-.12,0-.11A4.42,4.42,0,0,0,30,7.1a4,4,0,0,0-.35-0.64,3.87,3.87,0,0,0-3.22-1.71H5.53A3.87,3.87,0,0,0,2.32,6.48a4,4,0,0,0-.37.71,3.9,3.9,0,0,0-.21.71L2.24,8,1.72,8a2.56,2.56,0,0,0-.05.37v16a3.89,3.89,0,0,0,3.88,3.88h20.9a3.89,3.89,0,0,0,3.88-3.88v-16ZM27.6,9.5v14a2,2,0,0,1-2,2H6.41a2,2,0,0,1-2-2V9.5A2,2,0,0,1,4.47,9l10.45,8.37L15,17.47a1.53,1.53,0,0,0,.84.37h0.19A1.32,1.32,0,0,0,17,17.47L27.53,9A1.94,1.94,0,0,1,27.6,9.5ZM16,14.78L6.91,7.48H25.09Z"/></svg>
              <div className="Text">
                {user.email}
              </div>
            </div>
          </div>
        </div>
        <div className="OptionContainer">
          <div className="Container" onClick={()=>this.handleButton('likes')}>
            <div className="Option">
              <div className={this.state.activeButton==="likes" ? "TextContainer ActiveState": "TextContainer"} >
                {
                  this.state.activeButton==='likes' ? 
                  <>
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>like-outline</title><path id="Shape" d="M22,4a8,8,0,0,0-6,2.79A8,8,0,0,0,10,4a7.26,7.26,0,0,0-7.33,7.33c0,5,4.53,9.15,11.4,15.39L16,28.47l1.93-1.76c6.87-6.23,11.4-10.33,11.4-15.37A7.26,7.26,0,0,0,22,4h0Z"/></svg>
                    <div className="Text">
                      {this.state.likes} Likes
                    </div>
                  </>
                  :
                  <>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path id="Shape_2_" d="M22,4c-2.3,0-4.5,1.1-6,2.8C14.5,5.1,12.3,4,10,4c-4.1,0-7.3,3.2-7.3,7.3c0,5,4.5,9.1,11.4,15.4l1.9,1.7l1.9-1.8c6.9-6.2,11.4-10.3,11.4-15.4C29.3,7.2,26.1,4,22,4L22,4z M16.1,24.7L16,24.9l-0.1-0.1C9.5,19,5.3,15.2,5.3,11.3c0-2.7,2-4.7,4.7-4.7c2.1,0,4.1,1.3,4.8,3.1h2.5C17.9,8,19.9,6.7,22,6.7c2.7,0,4.7,2,4.7,4.7C26.7,15.2,22.5,19,16.1,24.7L16.1,24.7z"/></svg>
                    <div className="Text">
                      {this.state.likes} Likes
                    </div>
                  </>
                }
              </div>
            </div>
          </div>
          <div className="Container" onClick={()=>this.handleButton('going')}>
            <div className="Option Mid">
              <div className={this.state.activeButton==="going" ? "TextContainer ActiveState": "TextContainer"} >
                {
                  this.state.activeButton==='going' ? 
                  <>
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06Z"/></svg>          
                    <div className="Text">
                      {this.state.going} Going
                    </div>
                  </>
                  :
                  <>
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>check-outline</title><path d="M31.34,10.06L12.68,29.22a1,1,0,0,1-1.43,0L0.66,19.14a1,1,0,0,1,0-1.4L5,13.55a1,1,0,0,1,1.43,0L11.22,18,24.88,3.77a1,1,0,0,1,1.44,0l5,4.89A1,1,0,0,1,31.34,10.06ZM25.63,5.89L12,20.14a1,1,0,0,1-1.43,0L5.68,15.64,2.81,18.44l9.14,8.66L29.21,9.38Z"/></svg>        
                    <div className="Text">
                      {this.state.going} Going
                    </div>
                  </>
                }
              </div>
            </div>
          </div>
          <div className="Container" onClick={()=>this.handleButton('past')}>
            <div className="Option">
            <div className={this.state.activeButton==="past" ? "TextContainer ActiveState": "TextContainer"} >
                {
                  this.state.activeButton==='past' ? 
                  <>
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>past</title><path d="M0.72,13.52c-1.17,1.11-.87,3.51.67,5.36s3.74,2.44,4.91,1.33,0.87-3.51-.67-5.35S1.89,12.41.72,13.52Z"/><path d="M26.34,14.44c-1.38,2-1.46,4.41-.2,5.4S29.54,20,30.91,18s1.46-4.41.2-5.4S27.71,12.45,26.34,14.44Z"/><path d="M12.27,14c2.06-.51,3.14-3.67,2.4-7s-3-5.71-5.06-5.2S6.48,5.39,7.21,8.77,10.22,14.48,12.27,14Z"/><path d="M22.62,1.72c-2.06-.51-4.32,1.82-5.06,5.2s0.34,6.54,2.4,7,4.32-1.82,5.06-5.2S24.68,2.23,22.62,1.72Z"/><path d="M16,16.83c-4.91,0-8.6,8-8.6,10.67,0,6.52,3.69-.21,8.6-0.21s8.6,6.73,8.6.21C24.6,24.86,20.92,16.83,16,16.83Z"/></svg>                    <div className="Text">
                      {this.state.past} Past
                    </div>
                  </>
                  :
                  <>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" ><path d="M9,30.3c-0.3,0-0.6-0.1-0.8-0.2c-1-0.6-1.2-2.1-1.2-3.4c0-2.7,3.6-10.9,9-10.9c5.4,0,9,8.2,9,10.9c0,1.4-0.1,2.9-1.2,3.4c-1.1,0.6-2.2-0.1-3.5-0.9c-1.2-0.8-2.7-1.7-4.3-1.7s-3.1,0.9-4.3,1.7C10.7,29.8,9.8,30.3,9,30.3z M16,25.5c2.1,0,4,1.2,5.4,2c0.5,0.3,1.1,0.7,1.5,0.8c0.1-0.2,0.2-0.6,0.2-1.6c0-2.1-3.2-8.9-7-8.9c-3.8,0-7,6.8-7,8.9c0,1,0.1,1.4,0.2,1.6c0.3-0.1,1-0.5,1.5-0.8C12,26.7,13.9,25.5,16,25.5z M5.6,21.4c-0.1,0-0.1,0-0.2,0c-1.3-0.1-2.7-0.9-3.7-2.1c-1.8-2.2-2-4.9-0.5-6.3c0.6-0.6,1.4-0.8,2.3-0.8c1.3,0.1,2.7,0.9,3.8,2.1c1.8,2.1,2,4.9,0.5,6.3C7.1,21.2,6.4,21.4,5.6,21.4z M3.2,14.2c-0.3,0-0.5,0.1-0.7,0.2l0,0C1.9,15,2,16.6,3.2,18c0.7,0.8,1.6,1.3,2.3,1.4c0.3,0,0.6-0.1,0.8-0.2c0.6-0.6,0.4-2.2-0.7-3.6C4.9,14.8,4,14.2,3.2,14.2C3.2,14.2,3.2,14.2,3.2,14.2z M26.6,21c-0.7,0-1.3-0.2-1.8-0.6c-1.6-1.3-1.6-4.1,0-6.4c0.9-1.4,2.3-2.3,3.6-2.5c0.9-0.1,1.7,0.1,2.3,0.6c1.6,1.3,1.6,4.1,0,6.4c-0.9,1.4-2.3,2.3-3.6,2.5C26.9,20.9,26.8,21,26.6,21zM28.9,13.5c-0.1,0-0.1,0-0.2,0c-0.8,0.1-1.6,0.7-2.2,1.6v0c-1,1.5-1,3.2-0.4,3.7c0.2,0.2,0.5,0.2,0.8,0.2c0.8-0.1,1.6-0.7,2.2-1.6c1-1.5,1-3.2,0.4-3.7C29.3,13.5,29.1,13.5,28.9,13.5z M12,15.2c-0.7,0-1.4-0.2-2-0.6c-1.5-0.9-2.7-2.8-3.2-5.1c-0.8-3.7,0.5-7.1,3-7.7c0.9-0.2,1.8,0,2.7,0.5c1.5,0.9,2.7,2.8,3.2,5.1c0.8,3.7-0.5,7.1-3,7.7l0,0C12.5,15.2,12.3,15.2,12,15.2zM10.6,3.7c-0.1,0-0.2,0-0.3,0C9.1,4.1,8.2,6.3,8.8,9.1c0.4,1.7,1.2,3.2,2.3,3.8c0.4,0.3,0.9,0.4,1.2,0.3c1.2-0.3,2.1-2.6,1.5-5.3c-0.4-1.7-1.3-3.2-2.3-3.8C11.2,3.8,10.9,3.7,10.6,3.7z M20.2,15.1c-0.2,0-0.5,0-0.7-0.1c-2.5-0.6-3.8-3.9-3-7.7c0.5-2.3,1.7-4.2,3.2-5.1c0.9-0.6,1.8-0.7,2.8-0.5l0,0c2.5,0.6,3.8,4,3,7.7c-0.5,2.2-1.7,4.1-3.1,5C21.6,14.9,20.9,15.1,20.2,15.1zM21.6,3.7c-0.3,0-0.6,0.1-0.9,0.3c-1,0.6-1.9,2.1-2.3,3.8c-0.6,2.8,0.3,5.1,1.5,5.3c0.4,0.1,0.8,0,1.3-0.3c1-0.6,1.9-2.1,2.2-3.8c0.6-2.8-0.3-5-1.5-5.3l0,0C21.8,3.7,21.7,3.7,21.6,3.7z"/></svg>                    
                    <div className="Text">
                      {this.state.past} Past
                    </div>
                  </>
                }
              </div>
            </div>
          </div>  
        </div>
        {
          this.state.activePosts &&
          this.state.activePosts.map((post) => (
            <div className="Post" key={post._id}>
              <div className="Header Clearfix">
                {post && <HeaderItem singlePost={post}/>}
                <div className="PostChannel">
                  {post.channel}
                </div>
              </div>
              {
                post.image.length===0 ?
                <div className="PostDetailContainer Clearfix">
                  <div className="PostTitle">
                    {post.title}
                  </div>
                  <div className="Clearfix">
                    <svg className="TimeLogo" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path d="M21.2,21.6l-1.9,1.9l0,0l-5.6-5.5V7.6h2.7v9.2L21.2,21.6z M30.2,16.3c0,7.8-6.3,14.2-14.2,14.2 S1.8,24.2,1.8,16.3C1.8,8.5,8.2,2.2,16,2.1c0,0,0,0,0,0C23.8,2.2,30.2,8.5,30.2,16.3z M27.5,16.3C27.5,10,22.3,4.9,16,4.9 C9.7,4.9,4.5,10,4.5,16.3c0,6.3,5.1,11.5,11.5,11.5C22.3,27.8,27.5,22.7,27.5,16.3z"/></svg>
                    <div className="TimeText">
                      {post.start_date} {post.start} - {post.end_date} {post.end}
                    </div>
                  </div>
                  <div className="PostDetail">
                    {post.detail.length>150 ? post.detail.slice(0, 150) + '...' : post.detail }
                  </div>
                </div>
                :
                <div className="PostDetailContainer Clearfix">
                  <div className="LeftDetail">
                    <div className="PostTitle">
                      {post.title}
                    </div>
                    <div className="Clearfix">
                      <svg className="TimeLogo" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32"><path d="M21.2,21.6l-1.9,1.9l0,0l-5.6-5.5V7.6h2.7v9.2L21.2,21.6z M30.2,16.3c0,7.8-6.3,14.2-14.2,14.2 S1.8,24.2,1.8,16.3C1.8,8.5,8.2,2.2,16,2.1c0,0,0,0,0,0C23.8,2.2,30.2,8.5,30.2,16.3z M27.5,16.3C27.5,10,22.3,4.9,16,4.9 C9.7,4.9,4.5,10,4.5,16.3c0,6.3,5.1,11.5,11.5,11.5C22.3,27.8,27.5,22.7,27.5,16.3z"/></svg>
                      <div className="TimeText">
                        {post.start_date} - {post.end_date}
                      </div>
                    </div>
                    <div className="PostDetail">
                      {post.detail.length>150 ? post.detail.slice(0, 150) + '...' : post.detail }
                    </div>
                  </div>
                  <div className="ImageContainer">
                    <img src={post.image[0].imageUrl} alt={post.image[0].imageUrl} />
                  </div>
                </div>
              }
              <div className="PostButton Clearfix">
                <Going list={post.going} PId={post._id} updateComponent={this.updateComponent} />
                <Like list={post.likes} PId={post._id} updateComponent={this.updateComponent} />
              </div>
            </div>
          ))        
        }
        {this.state.activePosts.length <1 && 
          <div className="Empty">
            <div className="Container">
              <svg className="LoadingIcon" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><title>no-activity</title><path d="M16,32A16,16,0,0,1,2.17,8h0a16.16,16.16,0,0,1,1.33-2h0A16,16,0,1,1,16,32ZM30,16a13.85,13.85,0,0,0-.16-1.85A13.5,13.5,0,0,0,28,14a13.86,13.86,0,0,0-6.23,1.52,24,24,0,0,1,4.64,9.75A13.89,13.89,0,0,0,30,16ZM24.7,26.91A22.32,22.32,0,0,0,20.06,16.5,14,14,0,0,0,14,28a13.6,13.6,0,0,0,.15,1.84A13.8,13.8,0,0,0,16,30,13.87,13.87,0,0,0,24.7,26.91ZM12.11,29.4c0-.46-0.09-0.93-0.09-1.4a16,16,0,0,1,6.72-13A22.78,22.78,0,0,0,17,13.33,16,16,0,0,1,4,20c-0.47,0-.92,0-1.38-0.09A14,14,0,0,0,12.11,29.4ZM4.47,8.09A13.92,13.92,0,0,0,2,16a13.77,13.77,0,0,0,.19,1.86A13.52,13.52,0,0,0,4,18a14,14,0,0,0,11.39-5.91A22.31,22.31,0,0,0,4.47,8.09Zm13.4-5.91A13.84,13.84,0,0,0,16,2,13.94,13.94,0,0,0,6,6.28a24.37,24.37,0,0,1,10.45,4.09A13.85,13.85,0,0,0,18,4,13.64,13.64,0,0,0,17.88,2.19Zm2.06,0.44C20,3.08,20,3.53,20,4a15.93,15.93,0,0,1-2,7.68,25.06,25.06,0,0,1,2.36,2.24A15.92,15.92,0,0,1,28,12c0.47,0,.94,0,1.4.09A14,14,0,0,0,19.93,2.62Z" transform="translate(-0.02 0)"/></svg>
              <div className="text">No activity Found</div>
            </div>
          </div>
        }
      </>
    );
  }
}

Profile.propTypes = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  getPosts : PropTypes.func.isRequired,
  getPost : PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile : state.profile,
  auth : state.auth,
  post : state.post
});

const mapDispatchToProps={
  getPosts, getPost
  // resetFilter, loadUser, getPost, filterPosts, getPosts,getProfiles,getPostScroll,getProfileById
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile); 