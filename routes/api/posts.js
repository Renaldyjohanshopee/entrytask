const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');
const Post = require('../../models/Post');
const User = require('../../models/User');

router.get('/pagedposts', auth, async (req, res) => {
  try {
    const posts = await Post.find().sort({ date: -1 });
    const page = parseInt(req.query.page)
    const limit = parseInt(req.query.limit)
  
    const startIndex = (page -1)*limit
    const endIndex = page*limit
  
    const result = {}
  
    if (endIndex < posts.length){
      result.next = {
        page: page+1,
        limit : limit
      }
    }
  
    if (startIndex>0){
      result.previous = {
        page:page-1,
        limit:limit
      }
    }
  
    result.result = posts.slice(startIndex, endIndex)
  
    res.paginatedResult = result;
    res.json(res.paginatedResult);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/filteredpagedposts', auth, async (req, res) => {
  try {
    let posts = await Post.find().sort({ date: -1 });
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const channel = req.query.channel;
    let date = req.query.date;
    let start = req.query.start;
    let end = req.query.end;
    let filteredPosts=[];
    let today = new Date()
    if (date){
      if (date==="today"){
        filteredPosts = posts.filter(
          post => today.getDate()===post.event.getDate() &&
          post.event.getMonth()===today.getMonth() &&
          post.event.getYear()===today.getYear()  
        );
      }
      if(date==="tomorrow"){
        filteredPosts = posts.filter(
          post => post.event.getDate()===(today.getDate()+1) &&
          post.event.getMonth()===today.getMonth() &&
          post.event.getYear()===today.getYear()  
        )
      }
      if (date==="thisweek"){
        let dayOfWeek = today.getDay();
        let numDay = today.getDate();
        let startweek = new Date(today); 
        startweek.setDate(numDay - dayOfWeek);
        startweek.setHours(0, 0, 0, 0);
        let endweek = new Date(today); 
        endweek.setDate(numDay + (7 - dayOfWeek));
        endweek.setHours(0, 0, 0, 0);
        filteredPosts = posts.filter(
          post => post.event>=startweek &&
          post.event<=endweek
        )
      }      
      if (date==="thismonth"){
        filteredPosts = posts.filter(
          post => post.event.getMonth()===today.getMonth() &&
          post.event.getYear()===today.getYear()  
        )        
      }
      if (date==="later"){
        startDate= new Date(start);
        endDate= new Date (end);
        endDate.setHours(23,59,59);
        filteredPosts = posts.filter(
          post => post.event>=startDate && post.event<=endDate
        )
      }
    }    

    if (channel){
      
      if (date){
        filteredPosts = filteredPosts.filter(post => post.channel.replace(/\s/g, '')===channel.toLowerCase());
      }
      else{
        filteredPosts = posts.filter(post => post.channel.replace(/\s/g, '')===channel.toLowerCase());
      }
    }

    if(!channel&&!date){
      filteredPosts = posts;
    }

    const startIndex = (page -1)*limit
    const endIndex = page*limit
  
    const result = {}
  
    if (endIndex < filteredPosts.length){
      result.next = {
        page: page+1,
        limit : limit
      }
    }
  
    if (startIndex>0){
      result.previous = {
        page:page-1,
        limit:limit
      }
    }
  
    result.result = filteredPosts.slice(startIndex, endIndex)
  
    res.paginatedResult = result;
    res.json(res.paginatedResult);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/filteredposts', auth, async (req, res) => {
  try {
    let posts = await Post.find().sort({ date: -1 });
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const channel = req.query.channel;
    let date = req.query.date;
    let start = req.query.start;
    let end = req.query.end;
    let filteredPosts=[];
    let today = new Date()
    if (date){
      if (date==="today"){
        filteredPosts = posts.filter(
          post => today.getDate()===post.event.getDate() &&
          post.event.getMonth()===today.getMonth() &&
          post.event.getYear()===today.getYear()  
        );
      }
      if(date==="tomorrow"){
        filteredPosts = posts.filter(
          post => post.event.getDate()===(today.getDate()+1) &&
          post.event.getMonth()===today.getMonth() &&
          post.event.getYear()===today.getYear()  
        )
      }
      if (date==="thisweek"){
        let dayOfWeek = today.getDay();
        let numDay = today.getDate();
        let startweek = new Date(today); 
        startweek.setDate(numDay - dayOfWeek);
        startweek.setHours(0, 0, 0, 0);
        let endweek = new Date(today); 
        endweek.setDate(numDay + (7 - dayOfWeek));
        endweek.setHours(0, 0, 0, 0);
        filteredPosts = posts.filter(
          post => post.event>=startweek &&
          post.event<=endweek
        )
      }      
      if (date==="thismonth"){
        filteredPosts = posts.filter(
          post => post.event.getMonth()===today.getMonth() &&
          post.event.getYear()===today.getYear()  
        )        
      }
      if (date==="later"){
        startDate= new Date(start);
        endDate= new Date (end);
        filteredPosts = posts.filter(
          post => post.event>=startDate && post.event<=endDate
        )
      }
    }    

    if (channel){
      if (date){
        filteredPosts = filteredPosts.filter(post => post.channel.replace(/\s/g, '')===channel.toLowerCase());
      }
      else{
        filteredPosts = posts.filter(post => post.channel.replace(/\s/g, '')===channel.toLowerCase());
      }
    }

    if(!channel&&!date){
      filteredPosts = posts;
    }

    res.json(filteredPosts);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const posts = await Post.find().sort({ date: -1 });
    res.json(posts);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});


router.get('/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!req.params.id.match(/^[0-9a-fA-F]{24}$/) || !post) {
      return res.status(404).json({ msg: 'Post not found' });
    }

    res.json(post);
  } catch (err) {
    console.error(err.message);

    res.status(500).send('Server Error');
  }
});

router.put('/like/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (post.likes.filter(like => like.user.toString() === req.user.id).length>0) {
      const removeIndex = post.likes.map(like=> like.user.toString()).indexOf(req.user.id);
      post.likes.splice(removeIndex,1);
    }
    else{
      post.likes.unshift({ user: req.user.id });
    }

    await post.save();

    return res.json(post.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.put('/going/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (post.going.filter(go => go.user.toString() === req.user.id).length>0) {
      const removeIndex = post.going.map(go=> go.user.toString()).indexOf(req.user.id);
      post.going.splice(removeIndex,1);
    }
    else{
      post.going.unshift({ user: req.user.id });
    }

    await post.save();

    return res.json(post.going);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.post(
  '/comment/:id',
  [
    auth,
    [
      check('text', 'Text is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const id = req.params.id;
      const post = await Post.find();
      let postId={};
      for ( let i=0 ; i<post.length; i++){
        if (post[i]._id.toString() === id){
          postId = post[i] ;
        }
      }
      const newComment = {
        user: req.user.id,
        text: req.body.text
      };
      
      postId.comments.unshift(newComment);

      await postId.save();

      res.json(postId.comments);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);


module.exports = router;